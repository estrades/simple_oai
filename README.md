_(ce dépôt est la version maintenue du précédent dépôt situé à cette adresse https://git.unistra.fr/gporte/simple_oai/-/blob/master/README.md)_

# Simple OAI - générateur d'entrepôt OAI pour BaseX

## Prérequis 

* [BaseX](http://www.basex.org/) (testé avec BaseX 923)
* OAI PMH : [Plus d'informations ici ](https://www.openarchives.org/pmh/)


-----

## Installation

déziper ou cloner ce projet dans {DOSSIER_BASEX}/webapp

-----

## Tests

### adresse de test
{domaine}/simple_oai/test/oai

### fichiers de test
{DOSSIER_BASEX}/webapp/simple_oai/test

-----

## Affichage

L'application utilise par défaut la feuille de style suivante :
XSL Transform to convert OAI 2.0 responses into XHTML By Christopher Gutteridge, University of Southampton, 2006

Cette feuille de style ne fonctionne bien qu'avec les métadonnées préfixées oai_dc

-----

## Utilisation 

### dans votre propre webapp

1.  Copier le fichier {DOSSIER_BASEX}/webapp/simple_oai/test/param-test.xml dans votre webapp

2. Personnaliser les paramètres

3. Appeler la fonction principale simple_oai:getOAI()

```
declare
%rest:GET
%output:method("xml")
%rest:query-param("verb", "{$verb}")
%rest:query-param("metadataPrefix", "{$prefix}")
%rest:query-param("identifier", "{$identifier}")
%rest:query-param("set", "{$set}")
%rest:query-param("from", "{$from}")
%rest:query-param("until", "{$until}")
%rest:path("/{PATH_TO_WEBAPP}/oai")
function mon_app:setOAI(
$verb,
$prefix,
$identifier,
$set,
$from,
$until){
	
	(: optionnel : appel de la XSLT
	<?xml-stylesheet type="text/xsl" href="/simple_oai/oai2.xsl"?>, :)
	
	simple_oai:getOAI(
		$verb, 
		$prefix, 
		$identifier, 
		$set, 
		$from,
		$until,
		doc('param-test.xml'), 
		<resources>mon_app:resources()</resources>)
};
```

4. Créer une fonction pour mettre en place les résultats, et retourner un fichier xml sur ce modèle

```
declare function mon_app:resources(){
	(: appel des fichiers à indexer dans l'OAI :)
	(: traitement, boucles, etc. :)
	(: format des données renvoyées :)

		<resource>
			<header xmlns="http://www.openarchives.org/OAI/2.0/" >
				<identifier/>
				<datestamp/>
				<setSpec/>
				<!-- etc. -->
			</header>
			<metadata xmlns="http://www.openarchives.org/OAI/2.0/">
				<!-- métadonnées oai_dc, oai_marc, didl, etc. -->
			</metadata>
		</resource>


};
```

-----

## Elements pris en charge

### Verbs

* Identify
* GetRecord
* ListRecords
* ListMetadataFormats
* ListIdentifiers
* ListSets


### Paramètres

* verb
* identifier
* metadataPrefix
* from
* until
* set


### Namespace

* oai = "http://www.openarchives.org/OAI/2.0/"* 
* dc = "http://purl.org/dc/elements/1.1/"* 
* oai_dc = "http://www.openarchives.org/OAI/2.0/oai_dc/"* 
* oai_marc = "http://www.openarchives.org/OAI/1.1/oai_marc"


#### Ajouter un namespace

dans {DOSSIER_BASEX}/webapp/simple_oai/simple_oai.xqm

declare namespace prefix = "http://namespace.org" ;

-----

## A faire

* resumption token 
* batch size
* Deleting strategy is "no" – recommended is persistent or transient.
* revoir <response> => ajouter autres paramètres
