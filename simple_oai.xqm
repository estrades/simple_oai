module namespace simple_oai = 'simple_oai';

import module namespace functx = "http://www.functx.com";

declare namespace oai = "http://www.openarchives.org/OAI/2.0/" ;
declare namespace dc = "http://purl.org/dc/elements/1.1/" ;
declare namespace oai_dc = "http://www.openarchives.org/OAI/2.0/oai_dc/" ;
declare namespace oai_marc = "http://www.openarchives.org/OAI/1.1/oai_marc" ;
(: declare here other metadata namespaces :)
(: declare namespace didl = "https://standards.iso.org/ittf/PubliclyAvailableStandards/MPEG-21_schema_files/did/" ;:)


(: index :)
declare 
%rest:GET
%output:method("html")
%output:encoding("UTF-8")
%rest:path("/simple_oai")
function simple_oai:index(){

	'Entrepôt OAI pour BaseX'

};


(: default xsl file :)
declare 
%rest:GET
%output:method("xml")
%output:encoding("UTF-8")
%output:omit-xml-declaration("no")
%rest:path("/simple_oai/oai2.xsl")
function simple_oai:XSL(){
	doc('oai2.xsl')
};


(: main function to call from your own webapp
See simple_oai/test/oai-test.xqm :)
declare function simple_oai:getOAI(
$verb, 
$prefix, 
$identifier, 
$set, 
$from,
$until,
$param, 
$resources){

	simple_oai:oaiBase(
		$verb, 
		$prefix, 
		$identifier, 
		$set, 
		$from, 
		$until, 
		$param, 
		$resources)
};

(: include your own oai repository parameters :)
declare function simple_oai:getOAIParam(
$param, 
$name){

	if($param//*[name()=$name])
	then($param//*[name()=$name])
	else(doc('param.xml')//*[name()=$name])
};

(: generate OAI-PMH node based on verbs :)
declare function simple_oai:oaiBase(
$verb, 
$prefix, 
$identifier, 
$set, 
$from,
$until,
$param, 
$resources){
	<OAI-PMH 
	xmlns="http://www.openarchives.org/OAI/2.0/" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
		<responseDate>{fn:current-dateTime()}</responseDate>
		<request verb="{$verb}">{simple_oai:getOAIParam($param, 'baseURL')}</request>
		{
			if($verb = 'Identify')
			then(simple_oai:oaiIdentifiy($param))
			else if($verb = 'ListSets')
			then(simple_oai:oaiListSets($param))
			else if($verb = 'ListMetadataFormats')
			then(simple_oai:oaiListMetadataFormats($param))
			else if($verb = 'ListIdentifiers')
			then(simple_oai:oaiListIdentifiers($prefix, $set, $from, $until, $param, $resources))
			else if($verb = 'ListRecords')
			then(simple_oai:oaiListRecords($prefix, $set, $from, $until, $param, $resources))
			else if($verb = 'GetRecord')
			then(simple_oai:oaiGetRecord($identifier, $prefix, $set, $from, $until, $param, $resources))
			else(simple_oai:oaiBadVerb($param))
		}		
	</OAI-PMH>
};

(::::::: VERBS :::::::)

(: verb = Identifiy :)
declare function simple_oai:oaiIdentifiy($param){
	simple_oai:getOAIParam($param, 'Identify')
};

(: verb = ListSets :)
declare function simple_oai:oaiListSets($param){
	simple_oai:getOAIParam($param, 'ListSets')
};

(: verb = ListMetadataFormats :)
declare function simple_oai:oaiListMetadataFormats($param){
	simple_oai:getOAIParam($param, 'ListMetadataFormats')
};

(: verb = ListIdentifiers :)
declare function simple_oai:oaiListIdentifiers(
$prefix, 
$set, 
$from,
$until,
$param, 
$resources){

	<ListIdentifiers xmlns="http://www.openarchives.org/OAI/2.0/" >{
		for $resource in $resources//*:resource
		let $resource := simple_oai:sortResources($set, $prefix, $from, $until, $resource)

		return 
			if($resource != '')
			then($resource//*:header)
			else()
	}</ListIdentifiers>
};

(: verb = ListRecords :)
declare function simple_oai:oaiListRecords(
$prefix, 
$set, 
$from,
$until,
$param, 
$resources){

	<ListRecords xmlns="http://www.openarchives.org/OAI/2.0/" >{
		for $resource in $resources//*:resource
		let $resource := simple_oai:sortResources($set, $prefix, $from, $until, $resource)

		return
			if($resource != '')
			then(
				<record>
					{$resource//*:header}
					{simple_oai:displayPrefixMetadata($prefix, $resource//*:metadata)}
				</record>
			)
			else()
	}</ListRecords>
};

(: verb = GetRecord :)
declare function simple_oai:oaiGetRecord(
$identifier,
$prefix, 
$set, 
$from,
$until,
$param, 
$resources){

	for $resource in $resources//*:resource
	where $resource//*:header/*:identifier = $identifier
	let $resource := simple_oai:sortResources($set, $prefix, $from, $until, $resource)

	return
		if($resource != '')
		then(
			<GetRecord xmlns="http://www.openarchives.org/OAI/2.0/">
			<record>
				{$resource//*:header}
				{simple_oai:displayPrefixMetadata($prefix, $resource//*:metadata)}
			</record>
			</GetRecord>
		)
		else()
};

(: bad verb :)
declare function simple_oai:oaiBadVerb($param){
	<error code="badVerb">Value of the verb argument is not a legal OAI-PMH verb, the verb argument is missing, or the verb argument is repeated.</error>
};


(::::::: PARAMETERS :::::::)

(: main function to sort results after parameters :)
declare function simple_oai:sortResources(
$set, 
$prefix,
$from,
$until,
$resource){

		let $resource := simple_oai:sortBySet($set, $resource)
		let $resource := simple_oai:sortByPrefix($prefix, $resource)
		let $resource := simple_oai:sortByFrom($from, $resource)
		let $resource := simple_oai:sortByUntil($until, $resource)

		return $resource
};

(: param = set :)
declare function simple_oai:sortBySet(
$set, 
$resource){

	if(empty($set) = xs:boolean(0) 
		and $set != '' 
		and $resource//*:setSpec = $set)
	then($resource)
	else if(empty($set) = xs:boolean(1) )
	then($resource)
	else()
};

(: param = metadataPrefix :)
declare function simple_oai:sortByPrefix(
$prefix, 
$resource){

	for $child in $resource//child::*
	return
		if(empty($prefix) = xs:boolean(0) 
			and $prefix != '' 
			and prefix-from-QName(node-name($child)) = $prefix)
		then($resource)
		else if(empty($prefix) = xs:boolean(1) )
		then($resource)
		else()
};

(: param = from :)
declare function simple_oai:sortByFrom(
$from, 
$resource){

	if(empty($from) = xs:boolean(0) 
		and $from != '' 
		and xs:date(simple_oai:formatDateFrom($from)) <= xs:date(simple_oai:formatDateUntil($resource//*:datestamp)))
	then($resource)
	else if(empty($from) = xs:boolean(1) )
	then($resource)
	else()
};

(: param = until :)
declare function simple_oai:sortByUntil(
$until, 
$resource){

	if(empty($until) = xs:boolean(0) 
		and $until != '' 
		and xs:date(simple_oai:formatDateFrom($resource//*:datestamp)) <= xs:date(simple_oai:formatDateUntil($until)))
	then($resource)
	else if(empty($until) = xs:boolean(1) )
	then($resource)
	else()
};



(::::::: FUNCTIONS :::::::)

(: display or hide metadata based on prefix :)
declare function simple_oai:displayPrefixMetadata(
$prefix, 
$resource){

	<metadata xmlns="http://www.openarchives.org/OAI/2.0/">
	{
	for $child in $resource//child::*
	return

		if(empty($prefix) = xs:boolean(0) 
			and $prefix != '' 
			and prefix-from-QName(node-name($child)) = $prefix)
		then($child)
		else if(empty($prefix) = xs:boolean(1) )
		then($child)
		else()
	}
	</metadata>
};

(: clean date 1 => set beginning date :)
declare function simple_oai:formatDateFrom($date){
	
	let $newDate := 
		if(matches($date,'^\D*(\d{4})\D*(\d{2})*\D*(\d{2})*\D*$') )
		then(
			if(matches($date,'^\D*(\d{4})\D*(\d{2})\D*(\d{2})\D*$') )
			then(replace($date,'^\D*(\d{4})\D*(\d{2})\D*(\d{2})\D*$', '$1-$2-$3'))
			else if(matches($date,'^\D*(\d{4})\D*(\d{2})\D*$') )
			then(replace($date,'^\D*(\d{4})\D*(\d{2})\D*$', '$1-$2-01'))
			else if(matches($date,'^\D*(\d{4})\D*$') )
			then(replace($date,'^\D*(\d{4})\D*$', '$1-01-01'))
		)
		else if (matches($date,'^\D*(\d{2})*\D*(\d{2})*\D*(\d{4})\D*$') )
		then(
			if(matches($date,'^\D*(\d{2})\D*(\d{2})\D*(\d{4})\D*$') )
			then(replace($date,'^\D*(\d{2})\D*(\d{2})\D*(\d{4})\D*$', '$3-$2-$1'))
			else if(matches($date,'^\D*(\d{2})\D*(\d{4})\D*$') )
			then(replace($date,'^\D*(\d{2})\D*(\d{4})\D*$', '$2-$1-01'))
			else if(matches($date,'^\D*(\d{4})\D*$') )
			then(replace($date,'^\D*(\d{4})\D*$', '$1-01-01'))
		)
		else('1000-01-01')

	return
	try {xs:date($newDate)}
	catch *{
		'1000-01-01'
	}
};

(: clean date 2 => set end date :)
declare function simple_oai:formatDateUntil($date){
	
	let $newDate := 
		if(matches($date,'^\D*(\d{4})\D*(\d{2})*\D*(\d{2})*\D*$') )
		then(
			if(matches($date,'^\D*(\d{4})\D*(\d{2})\D*(\d{2})\D*$') )
			then(replace($date,'^\D*(\d{4})\D*(\d{2})\D*(\d{2})\D*$', '$1-$2-$3'))
			else if(matches($date,'^\D*(\d{4})\D*(\d{2})\D*$') )
			then(functx:last-day-of-month(replace($date,'^\D*(\d{4})\D*(\d{2})\D*$', '$1-$2-01')))
			else if(matches($date,'^\D*(\d{4})\D*$') )
			then(replace($date,'^\D*(\d{4})\D*$', '$1-12-31'))
		)
		else if (matches($date,'^\D*(\d{2})*\D*(\d{2})*\D*(\d{4})\D*$') )
		then(
			if(matches($date,'^\D*(\d{2})\D*(\d{2})\D*(\d{4})\D*$') )
			then(replace($date,'^\D*(\d{2})\D*(\d{2})\D*(\d{4})\D*$', '$3-$2-$1'))
			else if(matches($date,'^\D*(\d{2})\D*(\d{4})\D*$') )
			then(functx:last-day-of-month(replace($date,'^\D*(\d{2})\D*(\d{4})\D*$', '$2-$1-01')))
			else if(matches($date,'^\D*(\d{4})\D*$') )
			then(replace($date,'^\D*(\d{4})\D*$', '$1-12-31'))
		)
		else('9999-12-31')

	return
	try {xs:date($newDate)}
	catch *{
		'9999-12-31'
	}
	
};