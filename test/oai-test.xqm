module namespace simple_oai.test = 'simple_oai.test';

import module namespace  simple_oai = "simple_oai" at "../simple_oai.xqm";

(: 
main function to call simple_oai
here with an adress to test it
:)
declare
%rest:GET
%rest:POST
%output:method("xml")
%rest:query-param("verb", "{$verb}")
%rest:query-param("metadataPrefix", "{$prefix}")
%rest:query-param("identifier", "{$identifier}")
%rest:query-param("set", "{$set}")
%rest:query-param("from", "{$from}")
%rest:query-param("until", "{$until}")
%rest:path("/simple_oai/test/oai")
function simple_oai.test:setOAI(
$verb, 
$prefix, 
$identifier, 
$set,
$from,
$until){

	<?xml-stylesheet type="text/xsl" href="/simple_oai/oai2.xsl"?>,	
	simple_oai:getOAI(
		$verb, 
		$prefix, 
		$identifier, 
		$set, 
		$from,
		$until,
		doc('param-test.xml'), 
		<resources>{simple_oai.test:resources()}</resources>)
};

(: test adress to see results withour XSL stylesheet :)
declare
%rest:GET
%output:method("xml")
%rest:query-param("verb", "{$verb}")
%rest:query-param("metadataPrefix", "{$prefix}")
%rest:query-param("identifier", "{$identifier}")
%rest:query-param("set", "{$set}")
%rest:query-param("from", "{$from}")
%rest:query-param("until", "{$until}")
%rest:path("/simple_oai/test/resources")
function simple_oai.test:testResources(
$verb, 
$prefix, 
$identifier, 
$set,
$from,
$until){

	simple_oai.test:resources()
};


(: test adress to do more tests :)
declare
%rest:GET
%rest:query-param("verb", "{$verb}")
%rest:query-param("metadataPrefix", "{$prefix}")
%rest:query-param("identifier", "{$identifier}")
%rest:query-param("set", "{$set}")
%rest:query-param("from", "{$from}")
%rest:query-param("until", "{$until}")
%rest:path("/simple_oai/test/tests")
function simple_oai.test:tests(
$verb, 
$prefix, 
$identifier, 
$set,
$from,
$until){

	(: test something :)
};


(:
generate resources
need to be customize after you oww data
:)
declare function simple_oai.test:resources(){
	(:
		<resource>
			<header xmlns="http://www.openarchives.org/OAI/2.0/" >
				
			</header>
			<metadata xmlns="http://www.openarchives.org/OAI/2.0/">

			</metadata>
		</resource>
	:)

		for $resource in doc('test.xml')//*:book
		return
			<resource>
				<header xmlns="http://www.openarchives.org/OAI/2.0/" >
					<identifier>oai:example.com:{string($resource/@id)}</identifier>
					<datestamp>{string($resource/*:add)}</datestamp>
					<setSpec>set:books</setSpec>
					{
					for $set in $resource/*:genre
					return
						<setSpec>set:books:{lower-case(replace(string($set), ' ', '_'))}</setSpec>
					}
				</header>
				<metadata xmlns="http://www.openarchives.org/OAI/2.0/">
					  <oai_dc:dc 
				          xmlns:dc="http://purl.org/dc/elements/1.1/"  
				          xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
				          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				          xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ 
				          http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
				        <dc:identifier>http://example.org/books/{string($resource/@id)}</dc:identifier>
				        <dc:title>{$resource/*:title}</dc:title>
				        <dc:creator>{$resource/*:author}</dc:creator>
				        <dc:date>{$resource/*:year}</dc:date>
				        <dc:type>text</dc:type>
				        <dc:source>my library</dc:source>
				        <dc:language>{string($resource/@lang)}</dc:language>
				        
				      </oai_dc:dc>
				      <!-- add here other formats -->	
				</metadata>
			</resource>
};
